# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    lowercase = False
    uppercase = False
    digit = False
    special = False
    special_chars = ["$", "!", "@"]
    pw_length = len(password)

    for char in password:
        if char.islower():
            lowercase = True
        elif char.isupper():
            uppercase = True
        elif char.isdigit():
            digit = True
        elif char in special_chars:
            special = True

    if lowercase == True and uppercase == True and digit == True and special == True and pw_length >=6 and pw_length <=12 :
        return True
    else:
        return False



print(check_password("!GoodPassword$%*"))
print(check_password(""))
print(check_password("Books1999"))
print(check_password("Books1999!"))
print(check_password("B99!"))
