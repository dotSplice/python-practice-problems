# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(list):
    if len(list) % 2 == 0:
        index_to_slice = int(len(list)/2)
    else:
        index_to_slice = int(len(list)/2)+1
    list1 = list[0:index_to_slice]
    list2 = list[index_to_slice:len(list)]
    print("list1:", list1)
    print("list2:", list2)

halve_the_list([1, 2, 3, 4])
halve_the_list([1, 2, 3, 4, 5])
halve_the_list([1, 2, 3])
halve_the_list([1, 2])
