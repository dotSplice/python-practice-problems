# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None
    else:
        values.sort()
        return values[len(values)-1]


print(max_in_list([1,2,100,0,10,12,3]))
