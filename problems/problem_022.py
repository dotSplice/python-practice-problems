# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    if is_workday == True and is_sunny == True:
        return ["laptop"]
    elif is_workday == True and is_sunny == False:
        return ["laptop", "umbrella"]
    elif is_workday == False and is_sunny == True:
        return ["surfboard"]
    else: return ["umbrella"]


print('workday and sunny', gear_for_day(True,True))
print('workday and rainy', gear_for_day(True,False))
print('weekend and sunny', gear_for_day(False,True))
print('weekend and rainy', gear_for_day(False,False))
