# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    dict1 = {}
    for char in s:
        if char not in dict1:
            dict1[char] = 1
        else:
            dict1[char] += 1
    no_duplicates_list = dict1.keys()
    answer = ""
    for item in no_duplicates_list:
        answer += item
    return answer

print(remove_duplicate_letters("abc"))
print(remove_duplicate_letters("abcabcc"))
print(remove_duplicate_letters("abbbccdddc"))
print(remove_duplicate_letters("abbbccdddcwwwerrrt"))
