# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

def sum_fraction_sequence(number):
    numerator = 1
    denomenator = 2
    sum = 0
    for x in range(number):
        sum += numerator/denomenator
        numerator += 1
        denomenator += 1
    return sum

print(sum_fraction_sequence(3))
print(1/2 + 2/3 + 3/4)
